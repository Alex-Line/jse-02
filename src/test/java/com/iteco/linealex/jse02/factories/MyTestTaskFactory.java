package com.iteco.linealex.jse02.factories;

import com.iteco.linealex.jse02.factories.TaskFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//Test variant of TaskFactory with added test data
public class MyTestTaskFactory implements TaskFactory {
    private List<String> tasks;

    public MyTestTaskFactory() {
        this.tasks = new ArrayList<>();
        fillTasks();
    }

    private void fillTasks(){
        try (BufferedReader reader = new BufferedReader( new FileReader( "src/main/resources/tasks.txt" ) )) {
            while (reader.ready()){
                tasks.add( reader.readLine() );
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean createTask() {
        System.out.println("ENTER NAME: ");
        Scanner scanner = new Scanner( System.in );
        String nameOfTask = scanner.nextLine();
        if(tasks.contains( nameOfTask )){
            System.out.println("THERE IS TASK " + nameOfTask + "! PLEASE TRY AGAIN");
            return false;
        }
        tasks.add( nameOfTask );
        System.out.println("[OK]");
        System.out.println("");
        return true;
    }

    @Override
    public void listAllTasks() {
        System.out.println("[TASK LIST]");
        for (int index = 0; index < tasks.size(); index++) {
            System.out.println( (index+1) + ". " + tasks.get( index ));
        }
        System.out.println("");
    }

    @Override
    public boolean removeTask() {
        System.out.println("ENTER NAME: ");
        Scanner scanner = new Scanner( System.in );
        String nameOfTask = scanner.nextLine();
        if(tasks.contains( nameOfTask )){
            tasks.remove( nameOfTask );
            System.out.println("[OK]");
            System.out.println("");
            return true;
        }
        System.out.println("THERE IS NOT SUCH TASK! PLEASE TRY AGAIN!\n");
        return false;
    }

    @Override
    public void removeAllTasks() {
        tasks.clear();
        System.out.println("[ALL TASKS REMOVED]");
    }
}
