help: Show all commands.
project-create: Create new project
project-clare: Remove all projects
project-list: Show all projects
project-remove: Remove selected project
project-select: There is not support yet :(
task-create: Create new task
task-clare: Remove all tasks
task-list: Show all tasks
task-remove: Remove selected task