package com.iteco.linealex.jse02.factories;

public interface TaskFactory {
    public boolean createTask();

    public void listAllTasks();

    public boolean removeTask();

    public void removeAllTasks();
}
