package com.iteco.linealex.jse02.factories;

public interface ProjectFactory {
    public boolean createProject();

    public void listAllProjects();

    public boolean removeProject();

    public void removeAllProjects();
}
