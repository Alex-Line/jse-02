package com.iteco.linealex.jse02;

import com.iteco.linealex.jse02.controllers.ConsoleController;
import com.iteco.linealex.jse02.controllers.MyConsoleController;

public class App {

    public static void main(String[] args) {
        //Create the implementation of ConsoleController
        ConsoleController controller = new MyConsoleController();
        controller.listenToConsole();
    }
}
