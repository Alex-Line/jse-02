# Task Manager JSE-02

## developer
Aleksandr Linev

E-mail: uranus_123@mail.ru

## Software

Maven 3.6.3
Java 1.8
JRE
IDE IntelliJ IDEA CE

## Build commands

maven clean
maven install
